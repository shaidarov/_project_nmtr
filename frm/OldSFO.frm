.LinkForm 'OldSFO' Prototype is 'RNAL_MBP'
.Group '���஡��'
.NameInList '���祭� �� ��ॢ������� � ����� ���殤���� ����� ���� �����'
.p 80
.f "NUL"

.var
  Mol
, Podr
, Podr_Kod
, Party
, sMC
, sNnom
, OCU
, sNomPrih
          : String  ;
  i
, Counter
          : Word    ;
  n
, m
          : integer ;
 _cPrih_MBPin
          : Comp    ;
  FlagRun
, isNMTR
          : Boolean ;
  dPrihod
          : Date    ;
  Kau
, KauName
          : ARRAY [1..10] of String;
.endvar

.Create  view Raz
 as select * from
  MBPin
, KatMBP
, KatPodr
, KatMol
, KatParty
, SpecMTR
, SaldTune

, synonym SpecMTR  SpecMTR_5
, synonym SaldTune SaldTune_5

, synonym MBPin    Prih_MBPin
, synonym MBPout   Prih_MBPout

where
((
      NRecIn                == MBPin.nrec
  and MBPin.cKatSopr        == KatSopr.nRec
  and MBPin.cMBP            == KatMBP.nRec

  and MBPin.cPodr           == KatPodr.nRec
  and MBPin.cMol            == KatMol.nRec
  and MBPin.cPartyF         == KatParty.nRec

  and 1110                  == SpecMTR.coTable
  and MBPin.cMBPOut         == SpecMTR.cSpec
  and SpecMTR.cSaldTune     == SaldTune.Nrec

  and 1110                  == SpecMTR_5.coTable
  and MBPin.cSpSopr         == SpecMTR_5.cSpec
  and SpecMTR_5.cSaldTune   == SaldTune_5.nRec
));

.fields
.endfields

.begin
 xlCreateExcelWithTemplate(TranslatePath('%StartPath%')+'XLS\F_MBP\OldSFO.xltx', TRUE );
 xlSetCellStringValue( '���祭� �� ��ॢ������� � ����� "���殤����" ����� ���� ����� '+ DateToStr(dFor, 'DD/MM/YYYY')   , 1, 1 , 1, 1);
 n := 4;
end.

.{CheckEnter CIRFILT
.}
.{CheckEnter CIRCOM
.{CheckEnter CIRGRP
.}
.{CheckEnter CIROPER
.begin

  isNMTR   := true      ;
  FlagRun  := true      ;
  dPrihod  := ZeroDate  ;
  sNomPrih := ''        ;


 if ( GetFirst MBPin   != tsOK )
   isNMTR := false ;                                                         // fBreak � fContinue ����� �� ��ࠡ��뢠��


 if (    sGetAttr(coMBPin, MBPin.nRec, '�ਧ��� ���')           = '�������ᮢ�'
      or sGetAttr(coMBPin, MBPin.nRec, '�ਧ��� ��� �� ᪫���') = '�������ᮢ�'
      or sGetAttr(coMBPin, MBPin.nRec, '�ਧ��� ���')           = '002 �������ᮢ�'
      or sGetAttr(coMBPin, MBPin.nRec, '�ਧ��� ��� �� ᪫���') = '002 �������ᮢ�'
    )
   isNMTR := false ;


 if ( GetFirst KatSopr  = tsOK )
 {
    dPrihod  := KatSopr.dOpr  ;
    sNomPrih := KatSopr.nSopr ;
 }
 else
 {
      _cPrih_MBPin := MBPin.nRec ;
       counter     := 0          ;

            Do
            {
          	  if (GetFirst Prih_MBPin Where (( _cPrih_MBPin == Prih_MBPin.nRec )) = tsOK )
              {
               	  if (    Prih_MBPin.inState = 3    //   mbpFrMCOp  = 3;   // ����㯫���� � ᪫��� ��, ��� �᪫�祭�� cSopr -> KatSopr.NRec
                       or Prih_MBPin.inState = 5    //   mbpNaklOp  = 5;   // ����㯫���� � ᪫��� ��, ��� �᪫�祭�� cMBPOut -> SpSopr.NRec
                       or Prih_MBPin.inState = 9    //   mbpOverOp  = 9;   // ���室������ ����誠
                     )
                  {
                    dPrihod  := Prih_MBPin.dIn ;
                    sNomPrih := Prih_MBPin.Nom ;
                    FlagRun  := false          ;
                  }
                    else
                          {
                            if (GetFirst Prih_MBPout Where (( Prih_MBPin.cMBPOut == Prih_MBPout.nRec )) != tsOK )
                              FlagRun   := false ;

                            _cPrih_MBPin  := Prih_MBPout.cMBPin ;

                                counter++;
                            if (counter > 100)
                              FlagRun   := false ;
                          }
              }
               else
                  FlagRun := false     ;
            }
            While (FlagRun)
 }


if ( dPrihod = ZeroDate )                       // �᫨ �� ��諨 ��室, �㤥� ����� �� �����
   dPrihod := date(01,01,2000)

if ( Year(dPrihod) + 1 >= Year(dFor) )
   isNMTR := false ;


if (isNMTR)
{
 Podr := Podr_Kod := Mol := Party := '';

 if ( GetFirst KatMBP   = tsOK )
 {
    sMC      := KatMBP.Name    ;
    sNNom    := KatMBP.nNumber ;
 }

 if ( GetFirst KatPodr  = tsOK )
 {
    Podr     := KatPodr.Name   ;
    Podr_Kod := KatPodr.Kod    ;
 }

 if ( GetFirst KatMol   = tsOK )
    Mol      := KatMol.Name    ;

 if ( GetFirst KatParty = tsOK )
    Party    := KatParty.Name  ;


 case MBPin.Instate of
   3:{
       if (GetFirst SpecMTR = tsOk)
  	   {
         if (GetFirst SaldTune = tsOk)
         {
           OCU    := SaldTune.Name
           Kau[1] := ShowKau (SaldTune.wObj, SpecMTR.cObj);

           for (i := 2; i < 10; i++)
           {
             Kau[i] := ShowKau (SaldTune.wKau[i-1], SpecMTR.cKau[i-1]);
           }
         }
  	   }
     };
   5:{
       if (GetFirst SpecMTR_5 = tsOk)
       {
         if (GetFirst SaldTune_5 = tsOk)
         {
           OCU    := SaldTune_5.Name
           Kau[1] := ShowKau (SaldTune_5.wObj, SpecMTR_5.cObj);

           for (i := 2; i < 10; i++)
           {
             Kau[i] := ShowKau (SaldTune_5.wKau[i-1], SpecMTR_5.cKau[i-1]);
           }
         }
  	   }
     };

   else {
           OCU    := '' ;
           for (i := 1; i < 10; i++)
           {
             Kau[i] := '' ;
           }
        };
 end;


 m := 1 ;

 xlSetCellStringValue( sMC                                                                      , n, m, n, m);  m++;
 xlSetCellStringValue( sNnom                                                                    , n, m, n, m);  m++;
 xlSetCellStringValue( DateToStr   (dPrihod, 'DD/MM/YYYY')                                      , n, m, n, m);  m++;
 xlSetCellStringValue( sNomPrih                                                                 , n, m, n, m);  m++;
 xlSetCellStringValue( DoubleToStr (Kol         , '[|-]3666666666666.888888888888')             , n, m, n, m);  m++;
 xlSetCellStringValue( DoubleToStr (Price       , '[|-]3666666666666.888888888888')             , n, m, n, m);  m++;
 xlSetCellStringValue( DoubleToStr (if(wGetTune('Fin.MBP.OstStoim') = 0, Stoim, Stoim - SumIzn)
                                                , '[|-]3666666666666.888888888888')             , n, m, n, m);  m++;
 xlSetCellStringValue( if(Perc > 0, String(Perc)+'%', ' ')                                      , n, m, n, m);  m++;
 xlSetCellStringValue( Podr_Kod                                                                 , n, m, n, m);  m++;
 xlSetCellStringValue( Podr                                                                     , n, m, n, m);  m++;
 xlSetCellStringValue( Mol                                                                      , n, m, n, m);  m++;
 xlSetCellStringValue( Party                                                                    , n, m, n, m);  m++;
 xlSetCellStringValue( OCU                                                                      , n, m, n, m);  m++;
 xlSetCellStringValue( if (Kau[1]  <> '' , Kau[1]  , '-')                                       , n, m, n, m);  m++;
 xlSetCellStringValue( if (Kau[1]  <> '' , Kau[2]  , '-')                                       , n, m, n, m);  m++;
 xlSetCellStringValue( if (Kau[1]  <> '' , Kau[3]  , '-')                                       , n, m, n, m);  m++;
 xlSetCellStringValue( if (Kau[1]  <> '' , Kau[4]  , '-')                                       , n, m, n, m);  m++;
 xlSetCellStringValue( if (Kau[1]  <> '' , Kau[5]  , '-')                                       , n, m, n, m);  m++;
 xlSetCellStringValue( if (Kau[1]  <> '' , Kau[6]  , '-')                                       , n, m, n, m);  m++;
 xlSetCellStringValue( if (Kau[1]  <> '' , Kau[7]  , '-')                                       , n, m, n, m);  m++;
 xlSetCellStringValue( if (Kau[1]  <> '' , Kau[8]  , '-')                                       , n, m, n, m);  m++;
 xlSetCellStringValue( if (Kau[1]  <> '' , Kau[9]  , '-')                                       , n, m, n, m);  m++;
 xlSetCellStringValue( if (Kau[1]  <> '' , Kau[10] , '-')                                       , n, m, n, m);  m++;

 n++;
}

end.
.}
.{CheckEnter CIRITOG
.}
.}
.begin
 xlKillExcel;
end.
.EndForm
